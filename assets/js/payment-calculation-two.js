var allFields = document.querySelectorAll('.form-builder--item');
const total_el = allFields[1].querySelector('input');
const init_payment = allFields[2].querySelector('input');
const est_ins_cov = allFields[3].querySelector('input');
const term_el = allFields[4].querySelector('input');
const payment_methods = allFields[5];
const pay_in_full = payment_methods.querySelector(
  '#_builder-form .col-12:nth-last-child(5) input'
);
const pay_in_full_card = allFields[6].querySelector(
  '#_builder-form .col-12:nth-child(7) input'
);
let display_el = document.querySelector('#palc_payment_amount');
payment_methods.querySelector('label').style.display = 'none';
total_el.addEventListener('input', function () {
  updateAmountDue();
});
init_payment.addEventListener('input', function (e) {
  updateAmountDue();
});
est_ins_cov.addEventListener('input', function () {
  updateAmountDue();
});
term_el.addEventListener('input', function () {
  updateAmountDue();
});
pay_in_full.addEventListener('input', function () {
  resetOtherCheckBox(pay_in_full, pay_in_full_card);
  updateAmountDue();
});
pay_in_full_card.addEventListener('input', function () {
  resetOtherCheckBox(pay_in_full_card, pay_in_full);
  updateAmountDue();
});
const parentInit = est_ins_cov.closest('.col-12');
const parentTermEl = term_el.closest('.col-12');
let monthlyCost = '';

// Full Pay Cash Discount
var cashdiscountPercentage = 0.97;
var cashdiscountAmount = 3;

// Full Pay Card Discount
var carddiscountPercentage = 0.95;
var carddiscountAmount = 5;

function updateAmountDue() {
  if (!display_el) {
    display_el = document.querySelector('#palc_payment_amount');
  }

  const n = total_el.value;
  if (pay_in_full.checked) {
    parentTermEl.style.display = 'none';
    init_payment.value = 0;
    monthlyCost =
      (n - est_ins_cov.value - init_payment.value) * cashdiscountPercentage;
    let checkVal =
      monthlyCost == 0
        ? `Your total cost with ${cashdiscountAmount}% discount could be <span> $${monthlyCost} </span>`
        : `Your total cost with ${cashdiscountAmount}% could be <span> $${monthlyCost.toFixed(
            2
          )} </span>`;
    display_el.innerHTML = checkVal;
  } else if (pay_in_full_card.checked) {
    parentTermEl.style.display = 'none';
    init_payment.value = 0;
    monthlyCost =
      (n - est_ins_cov.value - init_payment.value) * carddiscountPercentage;
    let checkVal =
      monthlyCost == 0
        ? `Your total cost with ${carddiscountAmount}% discount could be <span> $${monthlyCost} </span>`
        : `Your total cost with ${carddiscountAmount}% discount could be <span> $${monthlyCost.toFixed(
            2
          )} </span>`;
    display_el.innerHTML = checkVal;
  } else {
    parentTermEl.style.display = 'block';
    let t = parseInt(term_el.value, 10) || 1;
    let e = (n - est_ins_cov.value - init_payment.value) / t;
    let checkVal =
      e == 0
        ? `Your monthly payment could be  <span> $${e}/Month </span>`
        : `Your monthly payment could be  <span> $${e.toFixed(
            2
          )}/Month </span>`;
    display_el.innerHTML = checkVal;
  }
}

function resetOtherCheckBox(onChangeBox, willChangeBox) {
  if (onChangeBox.checked) {
    willChangeBox.checked = false;
  }
}
updateAmountDue();
