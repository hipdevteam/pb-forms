var allFields = document.querySelectorAll('.form-builder--item');
const total_el = allFields[1].querySelector('input');
const init_payment = allFields[2].querySelector('input');
const est_ins_cov = allFields[3].querySelector('input');
const term_el = allFields[4].querySelector('input');
const payment_methods = allFields[5];
const pay_in_full = payment_methods.querySelector(
  '#_builder-form .col-12:nth-last-child(4) .flex-col > div > div:first-child input'
);
const pay_in_full_card = allFields[5].querySelector(
  '#_builder-form .col-12:nth-last-child(4) .flex-col > div  div+div:last-child input'
);
let display_el = document.querySelector('#palc_payment_amount');
payment_methods.querySelector('label').style.display = 'none';

total_el.addEventListener('input', updateAmountDue);
init_payment.addEventListener('input', updateAmountDue);
est_ins_cov.addEventListener('input', updateAmountDue);
term_el.addEventListener('input', updateAmountDue);
pay_in_full.addEventListener('input', function () {
  resetOtherCheckBox(pay_in_full, pay_in_full_card);
  updateAmountDue();
});
pay_in_full_card.addEventListener('input', function () {
  resetOtherCheckBox(pay_in_full_card, pay_in_full);
  updateAmountDue();
});

const parentInit = est_ins_cov.closest('.col-12');
const parentTermEl = term_el.closest('.col-12');
let monthlyCost = '';

function updateAmountDue() {
  if (!display_el) {
    display_el = document.querySelector('#palc_payment_amount');
  }

  const n = total_el.value;
  const cashDiscount = discountConfig.cashDiscount;
  const cardDiscount = discountConfig.cardDiscount;

  if (pay_in_full.checked) {
    parentTermEl.style.display = 'none';
    init_payment.value = 0;
    monthlyCost =
      (n - est_ins_cov.value - init_payment.value) * cashDiscount.percentage;
    let checkVal =
      monthlyCost == 0
        ? `Your total cost with ${cashDiscount.amount}% discount could be <span> $${monthlyCost} </span>`
        : `Your total cost with ${cashDiscount.amount}% discount could be <span> $${monthlyCost.toFixed(
            2
          )} </span>`;
    display_el.innerHTML = checkVal;
  } else if (pay_in_full_card.checked) {
    parentTermEl.style.display = 'none';
    init_payment.value = 0;
    monthlyCost =
      (n - est_ins_cov.value - init_payment.value) * cardDiscount.percentage;
    let checkVal =
      monthlyCost == 0
        ? `Your total cost with ${cardDiscount.amount}% discount could be <span> $${monthlyCost} </span>`
        : `Your total cost with ${cardDiscount.amount}% discount could be <span> $${monthlyCost.toFixed(
            2
          )} </span>`;
    display_el.innerHTML = checkVal;
  } else {
    parentTermEl.style.display = 'block';
    let t = parseInt(term_el.value, 10) || 1;
    let e = (n - est_ins_cov.value - init_payment.value) / t;
    let checkVal =
      e == 0
        ? `Your monthly payment could be  <span> $${e}/Month </span>`
        : `Your monthly payment could be  <span> $${e.toFixed(
            2
          )}/Month </span>`;
    display_el.innerHTML = checkVal;
  }
}

function resetOtherCheckBox(onChangeBox, willChangeBox) {
  if (onChangeBox.checked) {
    willChangeBox.checked = false;
  }
}

// Initial calculation
updateAmountDue();
