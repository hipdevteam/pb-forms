function privacyDetails(site_url = "", companyName = "") {
  let agreement_label = document.querySelectorAll('.form-builder--item .in-r-c label');

  let privacy_policy = `<a href="${site_url}privacy-policy/" target="_blank">Privacy Policy</a>`;

  agreement_label[0].innerHTML = `I have read and accept the ${privacy_policy}.
  By providing my phone number, I agree to receive text messages from ${companyName}. Message and data rates may apply. Message frequency varies.`;
}


privacyDetails('#', 'companyName' );

let captcha = document.querySelector('[id^="captchaInput_"]').id;
if (captcha) {
  let getParentNode = document.querySelector(`#${captcha}`).parentNode;
  getParentNode.style.cssText = `
      position: relative; 
      height: 70px;
    `;
}

// reCaptcha script for resize
function rescaleCaptcha() {
  let elementById = document.querySelector('[id^="captchaInput_"]').id;
  let elParent = document.querySelector(`#${elementById}`).parentNode;
  let width = elParent.offsetWidth;
  console.log('new width', width);
  let scale;
  if (width < 307) {
    scale = width / 307;
  } else {
    scale = 1.0;
  }
  elParent.style.cssText = `
    transform: scale(${scale}); 
    -webkit-transform: scale(+scale+); 
    transform-origin: 0 0;
    -webkit-transform-origin: 0 0; `;
}

rescaleCaptcha();
window.addEventListener('resize', function (event) {
  rescaleCaptcha();
});
