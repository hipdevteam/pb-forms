function multiplePrivacyDetails(site_url = "", selector = "") {
  // Validate the site URL is not empty
  if (!site_url) {
    console.error("Site URL is required.");
    return;
  }

  // Validate the selector is provided
  if (!selector) {
    console.error("Selector is required.");
    return;
  }

  // Ensure the URL ends with a slash
  site_url = site_url.endsWith("/") ? site_url : `${site_url}/`;

  // Generate the Privacy Policy link
  const privacy_policy = `<a href="${site_url}privacy-policy/" target="_blank">Privacy Policy</a>`;

  // Get specific checkbox labels based on the provided selector
  let agreement_labels = document.querySelectorAll(`${selector} label`);

  // Loop through each label and modify only the targeted ones
  agreement_labels.forEach(label => {
    if (label.textContent.includes("Privacy Policy")) {
      label.innerHTML = label.innerHTML.replace("Privacy Policy", privacy_policy);
    } else {
      label.innerHTML += ` View our ${privacy_policy}.`;
    }
  });
}


// reCaptcha script for resize
function rescaleCaptcha() {
  let elementById = document.querySelector('[id^="captchaInput_"]').id;
  let elParent = document.querySelector(`#${elementById}`).parentNode;
  let width = elParent.offsetWidth;
  console.log('new width', width);
  let scale;
  if (width < 307) {
    scale = width / 307;
  } else {
    scale = 1.0;
  }
  elParent.style.cssText = `
    transform: scale(${scale}); 
    -webkit-transform: scale(+scale+); 
    transform-origin: 0 0;
    -webkit-transform-origin: 0 0; `;
}

rescaleCaptcha();
window.addEventListener('resize', function (event) {
  rescaleCaptcha();
});