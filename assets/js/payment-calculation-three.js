// Payment calculator js
var allFields = document.querySelectorAll(".form-builder--item");
const total_el = allFields[0].querySelector("input");
const init_payment = allFields[1].querySelector("input");
const est_ins_cov = allFields[2].querySelector("input");
const term_el = allFields[3].querySelector("input");
const pay_in_full_wrap = allFields[4];
const pay_in_full = pay_in_full_wrap.querySelector("input");
let display_el = document.querySelector("#palc_payment_amount");
pay_in_full_wrap.querySelector("label").style.display = "none";
total_el.addEventListener("input", function () {
    updateAmountDue();
});

init_payment.addEventListener("input", function (e) {
    updateAmountDue();
});

est_ins_cov.addEventListener("input", function () {
    updateAmountDue();
});
term_el.addEventListener("input", function () {
    updateAmountDue();
});
pay_in_full.addEventListener("input", function () {
    updateAmountDue();
});
const parentInit = est_ins_cov.closest(".col-12");
const parentTermEl = term_el.closest(".col-12");
const InitPayment = init_payment.closest(".col-12");
init_payment;
let monthlyCost = "";

var discountPercentage = 0.97;
var discountAmount = 3;

function updateAmountDue() {
    if (!display_el) {
        display_el = document.querySelector("#palc_payment_amount");
    }

    const n = total_el.value;
    if (pay_in_full.checked) {
        console.log(discountPercentage);
        console.log(discountAmount);
        parentTermEl.style.display = "none";
        InitPayment.style.display = "none";
        init_payment.value = 0;
        monthlyCost = (n - est_ins_cov.value - init_payment.value) * discountPercentage;
        let checkVal = monthlyCost == 0 ? `Your total cost with ${discountAmount}% discount could be <span> $${monthlyCost} </span>` : `Your total cost with ${discountAmount}% discount could be <span> $${monthlyCost.toFixed(2)} </span>`;
        display_el.innerHTML = checkVal;
    } else {
        parentTermEl.style.display = "block";
        InitPayment.style.display = "block";
        let t = parseInt(term_el.value, 10) || 1;
        let e = (n - est_ins_cov.value - init_payment.value) / t;
        let checkVal = e == 0 ? `Your monthly payment could be  <span> $${e}/Month </span>` : `Your monthly payment could be  <span> $${e.toFixed(2)}/Month </span>`;
        display_el.innerHTML = checkVal;
    }
}
updateAmountDue();
